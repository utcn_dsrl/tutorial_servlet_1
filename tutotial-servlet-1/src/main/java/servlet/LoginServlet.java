package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import entities.User;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// add code for GET method
		PrintWriter out = response.getWriter();
		out.println("hello");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if ((!username.equals("") && !password.equals(""))) {
			UserDao userDao = new UserDao();
			User user = userDao.getUser(username, password);
			if (user != null) {
				if (!user.isAdmin()) {
					createUserPage(response);
				} else {
					createAdminPage(response);
				}
			} else {
				response.sendRedirect("index.html");
			}
		}

	}

	private void createAdminPage(HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<html>");

		out.println("<head>");
		out.println("<title>User Page</title>");
		out.println("</head>");

		out.println("<body>");
		out.println("Admin page");
		out.println("</body>");
		out.println("</html>");
		out.close();
	}

	private void createUserPage(HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<html>");

		out.println("<head>");
		out.println("<title>User Page</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("user page");
		out.println("</body>");
		out.println("</html>");
		out.close();
	}
}
