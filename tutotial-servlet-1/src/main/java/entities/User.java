package entities;



public class User {
	
	 private int id;
	    private String username;
	    private String password;
	    private String role;

	    public String getRole() {
			return role;
		}

		public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public boolean isAdmin() {
	        return role.equals("admin");
	    }

	    public String getUsername() {
	        return username;
	    }

	    public void setUsername(String username) {
	        this.username = username;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

	    public void setRole(String role) {
	        this.role = role;
	    }
	
	 

}
